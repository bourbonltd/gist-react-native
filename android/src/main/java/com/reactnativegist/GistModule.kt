package com.reactnativegist

import android.app.Application
import android.util.Log
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import build.gist.data.model.Message
import build.gist.presentation.GistSdk
import build.gist.presentation.GistListener

class GistModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {

  private val GIST_TAG: String = "Gist"
  
  override fun getName(): String {
      return GIST_TAG
  }
  
  @ReactMethod
  fun setup(organizationId: String) {
    val application : Application = reactApplicationContext.getApplicationContext() as Application
    GistSdk.getInstance().clearListeners()
    GistSdk.getInstance().init(application, organizationId)

    GistSdk.getInstance().addListener(object : GistListener {
      override fun onMessageShown(message: Message) {
        Log.d(GIST_TAG, "Message shown: ${message.messageId}")
        val params = mapMessage(message)
        sendEvent(reactApplicationContext, "onMessageShown", params)
      }
  
      override fun onMessageDismissed(message: Message) {
        Log.d(GIST_TAG, "Message dismissed: ${message.messageId}")
        val params = mapMessage(message)
        sendEvent(reactApplicationContext, "onMessageDismissed", params)
      }
  
      override fun onAction(message: Message, currentRoute: String, action: String) {
        Log.d(GIST_TAG, "Message action from: ${message.messageId}")
        val params = mapMessage(message)
        params.putString("currentRoute", currentRoute)
        params.putString("action", action)
        sendEvent(reactApplicationContext, "onAction", params)
      }
  
      override fun onError(message: Message) {
        Log.d(GIST_TAG, "Message error: ${message.messageId}")
        val params = mapMessage(message)
        sendEvent(reactApplicationContext, "onError", params)
      }
  
      override fun embedMessage(message: Message, elementId: String) {
        Log.d(GIST_TAG, "Message embed is not implemented on React Native")
      }
    })

    Log.d(GIST_TAG, "Gist initialized")
  }

  // User Token

  @ReactMethod
  fun setUserToken(userToken: String) {
    GistSdk.getInstance().setUserToken(userToken)
  }

  @ReactMethod
  fun clearUserToken() {
    GistSdk.getInstance().clearUserToken()
  }

  // Topics

  @ReactMethod
  fun getTopics(promise: Promise) {
    val array = WritableNativeArray()
    GistSdk.getInstance().getTopics().forEach {
      array.pushString(it)
    }
    promise.resolve(array)
  }

  @ReactMethod
  fun subscribeToTopic(topic: String) {
    GistSdk.getInstance().subscribeToTopic(topic)
  }

  @ReactMethod
  fun unsubscribeFromTopic(topic: String) {
    GistSdk.getInstance().unsubscribeFromTopic(topic)
  }

  @ReactMethod
  fun clearTopics() {
    GistSdk.getInstance().clearTopics()
  }

  // Routing

  @ReactMethod
  fun setCurrentRoute(route: String) {
    GistSdk.getInstance().setCurrentRoute(route)
  }

  // Messages

  @ReactMethod
  fun dismissMessage() {
    GistSdk.getInstance().dismissMessage()
  } 


  // Utilities

  fun sendEvent(reactContext: ReactApplicationContext, eventName: String, params: WritableMap) {
    reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java).emit(eventName, params)
  }

  fun mapMessage(message: Message): WritableMap {
    val params = Arguments.createMap()
    params.putString("messageId", message.messageId)
    params.putString("instanceId", message.instanceId)
    params.putString("queueId", message.queueId)
    return params
  }
  
}
