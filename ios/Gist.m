#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(Gist, NSObject)

RCT_EXTERN_METHOD(setup:(NSString *)organizationId)
RCT_EXTERN_METHOD(setUserToken:(NSString *)userToken)
RCT_EXTERN_METHOD(clearUserToken)
RCT_EXTERN_METHOD(getTopics:(RCTPromiseResolveBlock)resolve
                  withRejecter:(RCTPromiseRejectBlock)reject)
RCT_EXTERN_METHOD(subscribeToTopic:(NSString *)topic)
RCT_EXTERN_METHOD(unsubscribeFromTopic:(NSString *)topic)
RCT_EXTERN_METHOD(clearTopics)
RCT_EXTERN_METHOD(setCurrentRoute:(NSString *)route)
RCT_EXTERN_METHOD(dismissMessage)

@end
