import Foundation
import React

@objc(GistEventEmitter)
class GistEventEmitter: RCTEventEmitter {
    public static var shared: GistEventEmitter?

    override init() {
        super.init()
        GistEventEmitter.shared = self
    }

    open override func supportedEvents() -> [String]! {
        return [
            "onMessageShown",
            "onMessageDismissed",
            "onAction",
            "onError"
        ]
    }
}
