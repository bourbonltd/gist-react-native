import Gist

@objc(Gist)
class GistReact: NSObject, GistDelegate {
    private var currentMessageInstanceId: String?
    
    @objc(setup:)
    func setup(organizationId: String) -> Void {
        DispatchQueue.main.async {
            Gist.shared.setup(organizationId: organizationId, logging: true)
            Gist.shared.delegate = self
        }
    }

    @objc(setUserToken:)
    func setUserToken(userToken: String) -> Void {
        Gist.shared.setUserToken(userToken)
    }
    
    @objc(clearUserToken)
    func clearUserToken() -> Void {
        Gist.shared.clearUserToken()
    }
    
    @objc(getTopics:withRejecter:)
    func getTopics(resolve: RCTPromiseResolveBlock, reject:RCTPromiseRejectBlock) -> Void {
        resolve(Gist.shared.getTopics())
    }
    
    @objc(subscribeToTopic:)
    func subscribeToTopic(topic: String) -> Void {
        Gist.shared.subscribeToTopic(topic)
    }
    
    @objc(unsubscribeFromTopic:)
    func unsubscribeFromTopic(topic: String) -> Void {
        Gist.shared.unsubscribeFromTopic(topic)
    }
    
    @objc(clearTopics)
    func clearTopics() -> Void {
        Gist.shared.clearTopics()
    }

    @objc(setCurrentRoute:)
    func setCurrentRoute(route: String) -> Void {
        Gist.shared.setCurrentRoute(route)
    }

    @objc(dismissMessage)
    func dismissMessage() -> Void {
        if let currentMessageInstanceId = currentMessageInstanceId {
            Gist.shared.dismissMessage(instanceId: currentMessageInstanceId)
        }
    }

    @objc static func requiresMainQueueSetup() -> Bool {
        return true
    }
    
    // Gist Delegate
    
    func messageShown(message: Message) {
        currentMessageInstanceId = message.instanceId
        GistEventEmitter.shared?.sendEvent(withName: "onMessageShown", body: createMessageResponse(message: message))
    }
    
    func messageDismissed(message: Message) {
        currentMessageInstanceId = nil
        GistEventEmitter.shared?.sendEvent(withName: "onMessageDismissed", body: createMessageResponse(message: message))
    }
    
    func messageError(message: Message) {
        GistEventEmitter.shared?.sendEvent(withName: "onError", body: createMessageResponse(message: message))
    }
    
    func action(message: Message, currentRoute: String, action: String) {
        var messageDict = createMessageResponse(message: message)
        messageDict["currentRoute"] = currentRoute
        messageDict["action"] = action
        GistEventEmitter.shared?.sendEvent(withName: "onAction", body: messageDict)
    }
    
    func embedMessage(message: Message, elementId: String) {
        debugPrint("Message embed is not implemented on React Native")
    }
    
    // Utilities
    
    func createMessageResponse(message: Message) -> [String: String?] {
        let messageDict: [String: String?] =
            ["messageId": message.messageId,
             "instanceId": message.instanceId,
             "queueId": message.queueId]
        return messageDict
    }
}
