import * as React from 'react';

import { NativeEventEmitter, NativeModules, StyleSheet, View, Text } from 'react-native';
import Gist from '@gistproduct/react-native';

export default function App() {

  React.useEffect(() => {
    Gist.setup("c6ff92b9-5607-4655-9265-f2588f7e3b58");
    Gist.setUserToken("ABC123");

    Gist.subscribeToTopic("announcements");
    Gist.subscribeToTopic("react-native");

    Gist.setCurrentRoute("home");

    const eventEmitter = new NativeEventEmitter(NativeModules.GistEventEmitter);
    eventEmitter.addListener('onMessageShown', (event) => {
      console.log("Gist onMessageShown:");
      console.log(event);
    });

    eventEmitter.addListener('onMessageDismissed', (event) => {
      console.log("Gist onMessageDismissed:");
      console.log(event);
    });

    eventEmitter.addListener('onAction', (event) => {
      console.log("Gist onAction:");
      console.log(event);
    });

    eventEmitter.addListener('onError', (event) => {
      console.log("Gist onError:");
      console.log(event);
    });

  }, []);

  return (
    <View style={styles.container}>
      <Text>Hello from Gist React!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
