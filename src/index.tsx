import { NativeModules } from 'react-native';

type GistType = {
  setup(organizationId: String): void;
  setUserToken(userToken: String): void;
  clearUserToken(): void;
  getTopics(): Promise<[String]>;
  subscribeToTopic(topic: String): void;
  unsubscribeFromTopic(topic: String): void;
  clearTopics(): void;
  setCurrentRoute(route: String): void;
  dismissMessage(): void;
};

const { Gist } = NativeModules;

export default Gist as GistType;
